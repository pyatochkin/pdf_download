from bs4 import BeautifulSoup
import re
import os
import requests
from clint.textui import progress
# If there is no DownloadedFiles folder, then create it automatically
download_location = r'/Users/petro.pulia@pl.ibm.com/Downloads'
if not os.path.exists(download_location):os.mkdir(download_location)
# Connect to the website and retrieve destination links to each file
URL = "https://eprint.iacr.org/2020/"
response = requests.get(URL)
soup = BeautifulSoup(response.text, 'html.parser')
links = soup.find_all('a', href=re.compile(r'(.pdf)'))
# Table with destination links to each file
file_list = []
for link in links:
    file_list.append("https://eprint.iacr.org" + link['href'])
# Download all files to a specified location.
# 'clint' package elements were used to add simple progress bar visible only when script executed via Terminal
for url in file_list:
    r = requests.get(url, stream=True)
    filename = url.split('/')[-1]
    with open(download_location + filename, 'wb') as f:
        total_length = int(r.headers.get('content-length'))
        for ch in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1):
            if ch:
                f.write(ch)
                f.flush()
print('Download Completed!!!')