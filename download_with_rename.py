import requests
from bs4 import BeautifulSoup
import sys
import os
# Connect to the website and retrieve destination links to each file
URL = "https://eprint.iacr.org/2020/"
response = requests.get(URL)
soup = BeautifulSoup(response.text, 'html.parser')
titles = soup.find_all('b', text=True)
# Handling recursion limit error
sys.setrecursionlimit(10000)
# Table with original titles of each file
title_list = []
for title in titles:
    title_list.append(str(title)[3:-4])
# print(title_list)
directory = '~/Desktop/books/pdfs'
os.chdir(directory)
try:
    for file in os.listdir(directory):
        dst = str(title_list) + ".pdf"
        os.rename(file, dst)
except OSError as exc:
    if exc.errno == 36:
        handle_filename_too_long()
    else:
        raise    
