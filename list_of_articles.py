import requests
from bs4 import BeautifulSoup
import sys
# Connect to the website and retrieve destination links to each file
URL = "https://eprint.iacr.org/2020/"
response = requests.get(URL)
soup = BeautifulSoup(response.text, 'html.parser')
titles = soup.find_all('b', text=True)
# Handling recursion limit error
sys.setrecursionlimit(10000)
# Table with original titles of each file
title_list = []
for title in titles:
    # title.encode('latin1').decode('cp1252')
    title_list.append(str(title)[3:-4])
with open('titles.txt', 'w') as output_file:
    for title in title_list:
        output_file.write(str(title) + '\n')